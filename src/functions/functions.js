import axios from 'axios'

const baseUrl = "https://productsapi-n5p2.onrender.com/api";
const basePath = "/v1/products/";


const allProducts= async (state) => {
    const petition = await axios.get(baseUrl + basePath)
    state(petition.data);

}
const selectProduct = async (id, state) => {
    const petition = await axios.get(baseUrl + basePath + `${id}`)
    state(petition.data);
    
}
export {
    allProducts,
    selectProduct

}
