import React from 'react'

import { AiOutlineGithub } from 'react-icons/ai'
import { FaGitlab } from 'react-icons/fa'

import Logo from '../images/logo.png'

import '../Styles/footer.css'

const Footer = () => {
  return (
    <footer>
      <div>
        <a href="#top" className="footer__logo"><img src={Logo} alt="" /></a>
        <div className="footer__socials">
          <a href="https://www.github.com/ijchavez" target="_blank" rel='noreferrer'><AiOutlineGithub/></a>
          <a href="https://www.gitlab.com/ijchavez" target="_blank" rel='noreferrer'><FaGitlab/></a>

        </div>
        <div className="footer__copyright">
          <small>&copy; Gerardo Chavez 2022</small>
        </div>
        
      </div>
      
    </footer>
  
  )

}

export default Footer