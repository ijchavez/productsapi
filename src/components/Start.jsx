import React, { useEffect, useState } from 'react'
import { allProducts } from '../functions/functions'
import {Card, Button, CardGroup } from 'react-bootstrap'

import '../Styles/start.css';

const Start = () => {
  const [ products, setProducts ] = useState(null)
  useEffect(() => {
    allProducts(setProducts)

  }, [])

  return (
    <CardGroup>
        {products !== null ? (
          products.map(product => (
            product.isBlock === false ? (
                <div className="product">
                  <Card border="light" style={{ width: '18rem' }}>
                    <Card.Img variant="top" src={product.image} />
                    <Card.Body>
                      <Card.Title style={{fontSize: '1rem' }}>{product.name}</Card.Title>
                      <Button variant="primary">
                        <div key = {product.id}>
                            <a style={{color: 'white', textDecoration: "none" }} href={`/products/${product.id}`}>Product Details</a>

                        </div>

                      </Button>

                    </Card.Body>

                  </Card>

              </div>

              ) : ('')
          
            )

          )

        ) : ('No products available')}

    </CardGroup>   

  )
  
}

export default Start