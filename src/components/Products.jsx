import React, { useEffect, useState }  from 'react'
import { useParams } from 'react-router-dom'
import { selectProduct } from '../functions/functions'

import { Card, Figure, Button } from 'react-bootstrap'
import '../Styles/products.css';

const Product = () => {
  const [products, setProduct] = useState(null);
  const params = useParams();
  
  useEffect(() => {
    selectProduct(params.id, setProduct);

  }, [params.id])
  return (
    <>
       <Button href="/">Go Back</Button>
      {products !== null ? (
        <div className = "product">
          <Figure>
            <Figure.Image
              width={640}
              height={480}
              alt="171x180"
              src={products.image}
            />
            <Figure.Caption>
              <Card.Text>
                    {products.description}

                </Card.Text>
                <Card.Text>
                    Price: ${products.price}

                </Card.Text>
            </Figure.Caption>
        </Figure>
        </div>
        

      ) : ('No character found')}

    </>
              
  )
  
}

export default Product