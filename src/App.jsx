import React from "react";
import { BrowserRouter, Routes, Route } from 'react-router-dom'



import Start from './components/Start';
import Products from './components/Products';
import Header from './components/Header'
import Footer from './components/Footer'

import "bootstrap/dist/css/bootstrap.min.css";
import './Styles/App.css';

function App() {
  return (
    <div className="App" >
      <Header />

      <BrowserRouter>
         <Routes>
           <Route 
              path="/" 
              element = {<Start />}
            
            />
           <Route 
              path="/products/:id" 
              element = {<Products/>}
            />
            
         </Routes>
        
      </BrowserRouter>
      <Footer />
    </div>

  );

}

export default App;
